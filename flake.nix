{

  description = "Flake for GPhotos Uploader.";

  nixConfig = {
    # https://nixos.org/manual/nix/unstable/command-ref/conf-file.html
    bash-prompt-suffix = "dev-shell# ";
    # https://app.cachix.org/cache/rychly
    substituters = "https://rychly.cachix.org";
    trusted-public-keys = "rychly.cachix.org-1:uVaqzEHGJ2WxhNWhNVJa8hosCDZen3kwGQPcKYiI6TM=";
  };

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let

        name = "gphotos-uploader";
        pkgs = nixpkgs.legacyPackages.${system};
        lib = nixpkgs.lib;
        sanitizedNarHash = lib.concatMapStrings (s: if lib.isList s then "" else s) (builtins.tail (builtins.split "[^[:alnum:]]+" self.narHash));

      in rec {

        packages = { #flake-utils.lib.flattenTree {
          ${name} = import ./. {
             inherit pkgs;
             version = builtins.substring 7 7 sanitizedNarHash;
          };
        };

        # nix build
        defaultPackage = packages.${name};

        apps.${name} = flake-utils.lib.mkApp {
          inherit name;
          drv = packages.${name};
          #exePath = "/bin/${name}"	# set by default
        };

        # nix run
        defaultApp = apps.${name};

        # nix develop
        devShell = pkgs.mkShell rec {
          inherit (defaultPackage) buildInputs nativeBuildInputs;
          shellHook = ''
            # versions
            echo "# SOFTWARE:" ${builtins.concatStringsSep ", " (map (x: x.name) ( buildInputs ++ nativeBuildInputs ))}
          '';
        };

      }
    );

}
