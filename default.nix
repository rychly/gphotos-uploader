{ pkgs ? import <nixpkgs> { }, stdenv ? pkgs.stdenv, lib ? pkgs.lib
, gradle_7 ? pkgs.gradle_7
, jre ? pkgs.jre
, perl ? pkgs.perl
, makeWrapper ? pkgs.makeWrapper
, version ? (builtins.substring 0 7 (builtins.readFile .git/ORIG_HEAD))
}:

let

  pname = "gphotos-uploader";

  className = "io.gitlab.rychly.gphotos_uploader.GPhotosUploader";

  outputHash = "sha256-RaAqIm0ptkQlqO8IDbeF8HSx1f4FeEV0+chq6oBZEE0=";

  gradle = gradle_7;
  gradleVersion = (builtins.parseDrvName gradle.name).version;

  src = lib.cleanSourceWith {
    filter = lib.cleanSourceFilter;
    src = lib.cleanSourceWith {
      filter = path: type:
        !(lib.pathIsDirectory (path) && baseNameOf (toString (path))
          == "build");
      src = ./.;
    };
  };

  # fake build to pre-download deps into fixed-output derivation
  deps = stdenv.mkDerivation {
    pname = "${pname}-deps";
    inherit version src outputHash;
    nativeBuildInputs = [ gradle perl ];
    buildPhase = ''
      export GRADLE_USER_HOME=$(mktemp -d)
      # workaround: hide welcome message, see https://github.com/gradle/gradle/issues/5213#issuecomment-445200225
      mkdir -p $GRADLE_USER_HOME/notifications/${gradleVersion}/
      touch $GRADLE_USER_HOME/notifications/${gradleVersion}/release-features.rendered
      # https://github.com/gradle/gradle/issues/4426
      ${lib.optionalString stdenv.isDarwin "export TERM=dumb"}
      gradle --no-daemon --no-watch-fs assemble -Pversioning.disable=true
    '';
    # perl code mavenizes pathes (com.squareup.okio/okio/1.13.0/a9283170b7305c8d92d25aff02a6ab7e45d06cbe/okio-1.13.0.jar -> com/squareup/okio/okio/1.13.0/okio-1.13.0.jar)
    installPhase = ''
      find $GRADLE_USER_HOME/caches/modules-2 -type f -regex '.*\.\(jar\|pom\)' \
        | perl -pe 's#(.*/([^/]+)/([^/]+)/([^/]+)/[0-9a-f]{30,40}/([^/\s]+))$# ($x = $2) =~ tr|\.|/|; "install -Dm444 $1 \$out/$x/$3/$4/$5" #e' \
        | sh
    '';
    dontFixup = true;	# fixupPhase would break a directory structure of the repository (e.g., move {man,doc,info} dirs into the share dir); see https://nixos.org/manual/nixpkgs/stable/#ssec-fixup-phase
    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
  };

in stdenv.mkDerivation {

  inherit pname src version;

  nativeBuildInputs = [ gradle perl makeWrapper ];

  buildPhase = ''
    export GRADLE_USER_HOME=$(mktemp -d)
    # workaround: hide welcome message, see https://github.com/gradle/gradle/issues/5213#issuecomment-445200225
    mkdir -p $GRADLE_USER_HOME/notifications/${gradleVersion}/
    touch $GRADLE_USER_HOME/notifications/${gradleVersion}/release-features.rendered
    # https://github.com/gradle/gradle/issues/4426
    ${lib.optionalString stdenv.isDarwin "export TERM=dumb"}
    # point to offline repo in both the build script and plugin settings
    # (sometimes, the repos are defined by configure([buildscript.repositories, project.repositories]) so we will add the offline repo both after the mavenCentral and into the repositories section)
    [ -w build.gradle ] && sed -i \
      -e "s#\(mavenCentral()\)#\1; maven { url '${deps}' };#g" \
      -e "s#\(repositories {\)#\1  maven { url '${deps}' };#g" \
      -e "s#^\(version\s*=\s*\).*\$#\1'${version}'#" \
      build.gradle
    [ -w settings.gradle ] && sed -i \
      -e "s#\(gradlePluginPortal()\)#\1; maven { url '${deps}' };#g" \
      -e "s#\(mavenCentral()\)#\1; maven { url '${deps}' };#g" \
      settings.gradle
    gradle --offline --no-daemon --no-watch-fs build -Pversioning.disable=true
  '';

  preInstall = ''
    mkdir ./target
    tar -xf $(ls ./build/distributions/*.tar | grep -vF -- -shadow- | head -1) -C ./target
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/{bin,share/java}
    mv ./target/*/lib/*.jar $out/share/java/	# move JARs from the distribution directory
    unset CLASSPATH
    for I in $out/share/java/*.jar; do
      CLASSPATH=$CLASSPATH''${CLASSPATH:+:}$I
    done
    makeWrapper ${jre}/bin/java $out/bin/${pname} --add-flags "-cp $CLASSPATH ${className}"
    runHook postInstall
  '';

  meta = with lib; {
    description = "Uploads missing media files from given directories into Google Photos and control their sharing";
    homepage = src.url;
    license = licenses.asl20;
    #maintainers = [ maintainers.rychly ];	# TODO: register as the package maintainer
    platforms = platforms.unix;
  };
}
